# Pet Tracking Application

This application requires JDK 21 and maven.

## Build and run tests

```
mvn clean verify
```

## Run service

The following command will run the service on the port 8080 by default.

```
mvn spring-boot:run
```

## Notes

While starting up, the application will self populate some Pets in the database.

Adding a new tracking data (position update) can be done by calling:

```
curl -X POST --location "http://localhost:8080/v1/tracker/position" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{
        "time": "2024-06-30T11:33:33",
        "trackerId": 1,
        "latitude": 0.100,
        "longitude": 0.02,
        "ssid": "WIFI"
        }'
```

This will save the current position update and depending if the tracker is connected to the WIFI or not, it will update
the Pet `inZone` field.

A paginated list of all position updates can be retrieved using:

```
curl -X GET --location "http://localhost:8080/v1/tracker/position?page=0&pageSize=10"
```

The aggregated count of pet outside the power saving zone grouped by pet type and tracker type can be retrieved using:
```
curl -X GET --location "http://localhost:8080/v1/tracker/pet/out-of-zone"
```