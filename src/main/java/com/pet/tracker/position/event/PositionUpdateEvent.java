package com.pet.tracker.position.event;

import com.pet.tracker.position.controller.api.PositionUpdate;
import org.springframework.context.ApplicationEvent;

public class PositionUpdateEvent extends ApplicationEvent {
    private PositionUpdate positionUpdate;

    public PositionUpdateEvent(Object source, PositionUpdate positionUpdate) {
        super(source);
        this.positionUpdate = positionUpdate;
    }

    public PositionUpdate getPositionUpdate() {
        return positionUpdate;
    }
}
