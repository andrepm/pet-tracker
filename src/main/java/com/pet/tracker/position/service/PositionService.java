package com.pet.tracker.position.service;

import com.pet.tracker.position.controller.api.PositionUpdate;
import com.pet.tracker.position.domain.Position;
import com.pet.tracker.position.event.PositionUpdateEvent;
import com.pet.tracker.position.repository.PositionRepository;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


//TODO good candidate to decouple via async messaging
@Service
@Transactional(readOnly = true)
public class PositionService {
    private final PositionRepository positionRepository;
    private final PositionDtoMapper positionDtoMapper;
    private final ApplicationEventPublisher applicationEventPublisher;

    public PositionService(PositionRepository positionRepository, PositionDtoMapper positionDtoMapper, ApplicationEventPublisher applicationEventPublisher) {
        this.positionRepository = positionRepository;
        this.positionDtoMapper = positionDtoMapper;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Transactional
    public void savePositionUpdate(PositionUpdate positionUpdate) {
        // simulate sending message to message broker
        applicationEventPublisher.publishEvent(new PositionUpdateEvent(this, positionUpdate));

        positionRepository.save(positionDtoMapper.fromDto(positionUpdate));
    }

    public List<PositionUpdate> getPositionUpdate(int page, int size) {
        Page<Position> pageContent = positionRepository.findAll(PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "id")));
        return positionDtoMapper.toDto(pageContent.getContent());
    }

}
