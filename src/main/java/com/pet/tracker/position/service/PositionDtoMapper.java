package com.pet.tracker.position.service;

import com.pet.tracker.position.controller.api.PositionUpdate;
import com.pet.tracker.position.domain.Position;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PositionDtoMapper {

    Position fromDto(PositionUpdate positionUpdate) {
        return new Position()
                .setLatitude(positionUpdate.getLatitude())
                .setLongitude(positionUpdate.getLongitude())
                .setSsid(positionUpdate.getSsid())
                .setTime(positionUpdate.getTime())
                .setTrackerId(positionUpdate.getTrackerId());
    }

    public List<PositionUpdate> toDto(List<Position> content) {
        return content.stream().map(position -> new PositionUpdate()
                        .setLatitude(position.getLatitude())
                        .setLongitude(position.getLongitude())
                        .setSsid(position.getSsid())
                        .setTime(position.getTime())
                        .setTrackerId(position.getTrackerId()))
                .toList();
    }
}
