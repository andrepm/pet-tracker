package com.pet.tracker.position.controller;

import com.pet.tracker.position.controller.api.PositionUpdate;
import com.pet.tracker.position.service.PositionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1/tracker/position")
public class PositionController {
    private final PositionService positionService;

    public PositionController(PositionService positionService) {
        this.positionService = positionService;
    }

    @PostMapping
    public ResponseEntity<Void> createPosition(@RequestBody PositionUpdate positionUpdate) {
        positionService.savePositionUpdate(positionUpdate);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity<List<PositionUpdate>> getAllPositions(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "50") int pageSize
    ) {
        return ResponseEntity.ok(positionService.getPositionUpdate(page, pageSize));
    }

}
