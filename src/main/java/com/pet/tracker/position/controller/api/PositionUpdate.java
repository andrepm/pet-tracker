package com.pet.tracker.position.controller.api;

import java.time.LocalDateTime;

public class PositionUpdate {

    private LocalDateTime time;
    private long trackerId;
    private double latitude;
    private double longitude;
    private String ssid;

    public LocalDateTime getTime() {
        return time;
    }

    public PositionUpdate setTime(LocalDateTime time) {
        this.time = time;
        return this;
    }

    public long getTrackerId() {
        return trackerId;
    }

    public PositionUpdate setTrackerId(long trackerId) {
        this.trackerId = trackerId;
        return this;
    }

    public double getLatitude() {
        return latitude;
    }

    public PositionUpdate setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public double getLongitude() {
        return longitude;
    }

    public PositionUpdate setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public String getSsid() {
        return ssid;
    }

    public PositionUpdate setSsid(String ssid) {
        this.ssid = ssid;
        return this;
    }
}
