package com.pet.tracker.position.repository;

import com.pet.tracker.position.domain.Position;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.data.repository.ListPagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PositionRepository extends ListCrudRepository<Position, Long>, ListPagingAndSortingRepository<Position, Long> {
}
