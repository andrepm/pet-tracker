package com.pet.tracker.position.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.time.LocalDateTime;

@Entity
public class Position {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private LocalDateTime time;
    private long trackerId;
    private double latitude;
    private double longitude;
    private String ssid;

    public Long getId() {
        return id;
    }

    public Position setId(Long id) {
        this.id = id;
        return this;
    }

    public long getTrackerId() {
        return trackerId;
    }

    public Position setTrackerId(long trackerId) {
        this.trackerId = trackerId;
        return this;
    }

    public double getLatitude() {
        return latitude;
    }

    public Position setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public double getLongitude() {
        return longitude;
    }

    public Position setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public String getSsid() {
        return ssid;
    }

    public Position setSsid(String ssid) {
        this.ssid = ssid;
        return this;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public Position setTime(LocalDateTime time) {
        this.time = time;
        return this;
    }
}
