package com.pet.tracker.management.controller;

import com.pet.tracker.management.controller.api.OutOfZone;
import com.pet.tracker.management.service.PetService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/tracker/pet")
public class PetController {
    private final PetService petService;

    public PetController(PetService petService) {
        this.petService = petService;
    }

    @GetMapping("out-of-zone")
    public ResponseEntity<OutOfZone> getOutOfZone() {
        return ResponseEntity.ok(petService.getOutOfZone());
    }

}
