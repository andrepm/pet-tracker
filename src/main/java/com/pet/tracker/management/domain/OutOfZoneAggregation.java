package com.pet.tracker.management.domain;

public interface OutOfZoneAggregation {
    String getPetAndTrackerType();

    long getCount();
}
