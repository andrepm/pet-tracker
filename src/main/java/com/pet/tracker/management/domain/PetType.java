package com.pet.tracker.management.domain;

public enum PetType {
    CAT, DOG
}
