package com.pet.tracker.management.domain;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

@Entity
@DiscriminatorValue("CAT")
public class Cat extends Pet {
    private boolean lostTracker;

    public boolean isLostTracker() {
        return lostTracker;
    }

    public void setLostTracker(boolean lostTracker) {
        this.lostTracker = lostTracker;
    }
}
