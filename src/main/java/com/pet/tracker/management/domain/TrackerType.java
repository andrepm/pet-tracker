package com.pet.tracker.management.domain;

public enum TrackerType {
    SMALL, MEDIUM, BIG
}
