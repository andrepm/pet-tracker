package com.pet.tracker.management.service;

import com.pet.tracker.management.domain.Pet;
import com.pet.tracker.management.repository.PetRepository;
import com.pet.tracker.position.controller.api.PositionUpdate;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

/**
 * Responsible to update Pet entity with the information whether the tracker is in power saving zone.
 */
@Service
@Transactional
public class PositionUpdateService {

    private final PetRepository petRepository;

    public PositionUpdateService(PetRepository petRepository) {
        this.petRepository = petRepository;
    }

    @CacheEvict("OUT_OF_ZONE")
    public void update(PositionUpdate positionUpdate) {
        Pet pet = petRepository.findById(positionUpdate.getTrackerId())
                .orElseThrow(() -> new IllegalArgumentException("Tracker id not found"));

        pet.setInZone(!ObjectUtils.isEmpty(positionUpdate.getSsid()));

        petRepository.save(pet);
    }

}
