package com.pet.tracker.management.service;

import com.pet.tracker.management.controller.api.OutOfZone;
import com.pet.tracker.management.domain.OutOfZoneAggregation;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PetDtoMapper {

    public OutOfZone toDto(List<OutOfZoneAggregation> outOfZoneAggregations) {
        OutOfZone outOfZone = new OutOfZone();
        outOfZoneAggregations.forEach(outOfZoneAggregation -> {
            outOfZone.put(outOfZoneAggregation.getPetAndTrackerType(), outOfZoneAggregation.getCount());
        });
        return outOfZone;
    }

}
