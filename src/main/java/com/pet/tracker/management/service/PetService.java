package com.pet.tracker.management.service;

import com.pet.tracker.management.controller.api.OutOfZone;
import com.pet.tracker.management.domain.OutOfZoneAggregation;
import com.pet.tracker.management.repository.PetRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class PetService {
    private final PetRepository petRepository;
    private final PetDtoMapper petDtoMapper;

    public PetService(PetRepository petRepository, PetDtoMapper petDtoMapper) {
        this.petRepository = petRepository;
        this.petDtoMapper = petDtoMapper;
    }

    @Cacheable("OUT_OF_ZONE")
    public OutOfZone getOutOfZone() {
        List<OutOfZoneAggregation> outOfZoneAggregations = petRepository.countTotalOutOfZoneByPetAndTrackerTypes();
        return petDtoMapper.toDto(outOfZoneAggregations);
    }

}
