package com.pet.tracker.management.event;

import com.pet.tracker.management.service.PositionUpdateService;
import com.pet.tracker.position.event.PositionUpdateEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class PositionUpdateListener implements ApplicationListener<PositionUpdateEvent> {

    private final PositionUpdateService positionUpdateService;

    public PositionUpdateListener(PositionUpdateService positionUpdateService) {
        this.positionUpdateService = positionUpdateService;
    }

    @Override
    public void onApplicationEvent(PositionUpdateEvent event) {
        positionUpdateService.update(event.getPositionUpdate());
    }
}
