package com.pet.tracker.management.repository;

import com.pet.tracker.management.domain.OutOfZoneAggregation;
import com.pet.tracker.management.domain.Pet;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.data.repository.ListPagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PetRepository extends ListCrudRepository<Pet, Long>, ListPagingAndSortingRepository<Pet, Long> {

    @Query(""" 
            SELECT CONCAT(p.petType, ' ', p.trackerType) AS petAndTrackerType, COUNT(p) AS count
            FROM Pet as p
            WHERE p.inZone = false 
            GROUP BY petAndTrackerType
            """)
    List<OutOfZoneAggregation> countTotalOutOfZoneByPetAndTrackerTypes();

}
