package com.pet.tracker.utils;

import com.pet.tracker.management.domain.Cat;
import com.pet.tracker.management.domain.Dog;
import com.pet.tracker.management.domain.TrackerType;

public class PetsUtils {
    public static Dog createDog(TrackerType trackerType) {
        return createDog(trackerType, true);
    }

    public static Dog createDog(TrackerType trackerType, boolean inZone) {
        Dog dog = new Dog();
        dog.setTrackerType(trackerType);
        dog.setOwnerId(1);
        dog.setInZone(inZone);
        return dog;
    }

    public static Cat createCat() {
        return createCat(true);
    }

    public static Cat createCat(boolean inZone) {
        Cat cat = new Cat();
        cat.setTrackerType(TrackerType.SMALL);
        cat.setOwnerId(1);
        cat.setInZone(inZone);
        cat.setLostTracker(false);
        return cat;
    }
}
