package com.pet.tracker.utils;

import com.pet.tracker.management.repository.PetRepository;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.stream.IntStream;

import static com.pet.tracker.management.domain.TrackerType.BIG;
import static com.pet.tracker.management.domain.TrackerType.MEDIUM;
import static com.pet.tracker.utils.PetsUtils.createCat;
import static com.pet.tracker.utils.PetsUtils.createDog;

@Component
public class LoadPets {
    private final PetRepository petRepository;

    public LoadPets(PetRepository petRepository) {
        this.petRepository = petRepository;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void loadPets() {
        IntStream.range(0, 5).forEach(i -> petRepository.save(createDog(BIG)));
        IntStream.range(0, 5).forEach(i -> petRepository.save(createCat()));
        IntStream.range(0, 5).forEach(i -> petRepository.save(createDog(MEDIUM, false)));
        IntStream.range(0, 3).forEach(i -> petRepository.save(createDog(BIG, false)));
        IntStream.range(0, 3).forEach(i -> petRepository.save(createCat(false)));
    }
}
