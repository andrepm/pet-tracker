package com.pet.tracker.position.controller;

import com.pet.tracker.TrackerApplicationTests;
import com.pet.tracker.management.controller.api.OutOfZone;
import com.pet.tracker.management.domain.Dog;
import com.pet.tracker.management.domain.TrackerType;
import com.pet.tracker.management.repository.PetRepository;
import com.pet.tracker.position.controller.api.PositionUpdate;
import com.pet.tracker.utils.PetsUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

class PositionControllerTest extends TrackerApplicationTests {
    @Autowired
    PetRepository petRepository;

    @Test
    void test_positionUpdatesPetInZone() {
        Dog dog = petRepository.save(PetsUtils.createDog(TrackerType.SMALL));
        assertThat(dog.isInZone()).isEqualTo(true);
        OutOfZone outOfZone = getOutOfZone();
        assertThat(outOfZone.get("DOG SMALL")).isNull();

        // dog getting out of zone
        PositionUpdate positionUpdate = new PositionUpdate()
                .setLongitude(1.0)
                .setLatitude(-0.3)
                .setSsid(null)
                .setTrackerId(dog.getId())
                .setTime(LocalDateTime.now());
        restTemplate.postForObject(getBaseUrl() + "/position", positionUpdate, Void.class);

        outOfZone = getOutOfZone();
        assertThat(outOfZone.get("DOG SMALL")).isEqualTo(1);

        // dog returning to zone
        positionUpdate.setSsid("WIFI");
        restTemplate.postForObject(getBaseUrl() + "/position", positionUpdate, Void.class);

        outOfZone = getOutOfZone();
        assertThat(outOfZone.get("DOG SMALL")).isNull();
    }

    private OutOfZone getOutOfZone() {
        return restTemplate.getForObject(getBaseUrl() + "/pet/out-of-zone", OutOfZone.class);
    }

    @Test
    void test_updatesPagination() {
        Dog dog = petRepository.save(PetsUtils.createDog(TrackerType.SMALL));
        PositionUpdate positionUpdate = new PositionUpdate()
                .setLongitude(1.0)
                .setLatitude(-0.3)
                .setSsid(null)
                .setTrackerId(dog.getId())
                .setTime(LocalDateTime.now());
        IntStream.range(0, 100).forEach(i -> restTemplate.postForObject(getBaseUrl() + "/position", positionUpdate, Void.class));

        PositionUpdate[] positions = restTemplate.getForObject(getBaseUrl() + "/position?pageSize=10", PositionUpdate[].class);
        assertThat(positions).hasSize(10);

    }

}