package com.pet.tracker;

import com.pet.tracker.management.repository.PetRepository;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.util.stream.IntStream;

import static com.pet.tracker.management.domain.TrackerType.BIG;
import static com.pet.tracker.management.domain.TrackerType.MEDIUM;
import static com.pet.tracker.utils.PetsUtils.createCat;
import static com.pet.tracker.utils.PetsUtils.createDog;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class TrackerApplicationTests {
    @LocalServerPort
    protected int port;

    @Autowired
    protected TestRestTemplate restTemplate;

    @Autowired
    PetRepository petRepository;

    @BeforeEach
    void setUp() {
        petRepository.deleteAll();
    }

    protected String getBaseUrl() {
        return "http://localhost:%d/v1/tracker/".formatted(port);
    }

    protected void populatePets() {
        IntStream.range(0, 5).forEach(i -> petRepository.save(createDog(BIG)));
        IntStream.range(0, 5).forEach(i -> petRepository.save(createCat()));
        IntStream.range(0, 5).forEach(i -> petRepository.save(createDog(MEDIUM, false)));
        IntStream.range(0, 3).forEach(i -> petRepository.save(createDog(BIG, false)));
        IntStream.range(0, 3).forEach(i -> petRepository.save(createCat(false)));
    }

}
