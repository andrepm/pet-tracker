package com.pet.tracker.management.controller;

import com.pet.tracker.TrackerApplicationTests;
import com.pet.tracker.management.controller.api.OutOfZone;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PetControllerTest extends TrackerApplicationTests {

    @Test
    void test_outOfZoneAggregation() {
        populatePets();

        OutOfZone outOfZone = restTemplate.getForObject(getBaseUrl() + "/pet/out-of-zone", OutOfZone.class);

        assertThat(outOfZone).hasSize(3);
        assertThat(outOfZone.get("CAT SMALL")).isEqualTo(3);
        assertThat(outOfZone.get("DOG BIG")).isEqualTo(3);
        assertThat(outOfZone.get("DOG MEDIUM")).isEqualTo(5);
    }
}