package com.pet.tracker.management.repository;

import com.pet.tracker.TrackerApplicationTests;
import com.pet.tracker.management.domain.OutOfZoneAggregation;
import com.pet.tracker.management.domain.Pet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.pet.tracker.management.domain.TrackerType.BIG;
import static com.pet.tracker.utils.PetsUtils.createCat;
import static com.pet.tracker.utils.PetsUtils.createDog;
import static org.assertj.core.api.Assertions.assertThat;

class PetRepositoryTest extends TrackerApplicationTests {

    @Autowired
    PetRepository petRepository;

    @BeforeEach
    void setUp() {
        petRepository.deleteAll();
    }

    @Test
    void test_createAndList() {
        petRepository.save(createDog(BIG));
        petRepository.save(createCat());

        List<Pet> allPets = petRepository.findAll();
        assertThat(allPets).hasSize(2);
    }

    @Test
    void test_groupOutOfZone() {
        populatePets();

        List<OutOfZoneAggregation> outOfZoneAggregations = petRepository.countTotalOutOfZoneByPetAndTrackerTypes();
        assertThat(outOfZoneAggregations)
                .hasSize(3);
    }
}